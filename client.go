package main

import (
	"bytes"
	"github.com/gorilla/websocket"
	"log"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
	tab     = []byte{'\t'}
)

type ChatClient struct {
	// subscribed room
	room *ChatRoom

	// client websocket connection
	conn *websocket.Conn

	// buffered channel of outbound messages
	send chan []byte

	username string
}

func (cl *ChatClient) messagePrefix() []byte {
	return append([]byte(cl.username), tab...)
}

func (cl *ChatClient) read() {
	defer func() {
		cl.room.unregister <- cl
		_ = cl.conn.Close()
	}()

	cl.conn.SetReadLimit(maxMessageSize)
	_ = cl.conn.SetReadDeadline(time.Now().Add(pongWait))

	cl.conn.SetPongHandler(func(string) error {
		_ = cl.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	for {
		_, message, err := cl.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}

		// escape newlines with spaces
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		cl.room.broadcast <- message
	}
}

func (cl *ChatClient) write() {
	pingTick := time.NewTicker(pingPeriod)
	defer func() {
		pingTick.Stop()
		_ = cl.conn.Close()
	}()

	// handle send channel and pingTick
	for {
		select {
		case msg, ok := <-cl.send:
			_ = cl.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				_ = cl.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := cl.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			_, _ = w.Write(append(cl.messagePrefix(), msg...))

			// also write queued messages
			n := len(cl.send)
			for i := 0; i < n; i++ {
				_, _ = w.Write(newline)
				_, _ = w.Write(append(cl.messagePrefix(), <-cl.send...))
			}

			if err := w.Close(); err != nil {
				return
			}

		case <-pingTick.C:
			_ = cl.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := cl.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func NewClient(room *ChatRoom, conn *websocket.Conn, username string) *ChatClient {
	return &ChatClient{
		room:     room,
		conn:     conn,
		username: username,
		send:     make(chan []byte, 256),
	}
}
