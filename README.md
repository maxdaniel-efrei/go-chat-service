# Go Chat Service

A simple chat server implementation.
Exposes an API to allow chatting.

## Installation

Refer to the `go.mod` file for the required versions of go and associated dependencies.

To get a binary, simply build with the following command while go is in the PATH:

```shell
go build .
```

This should create a binary named `chatservice`, or `chatservice.exe` if on Windows.

## Usage

Simply running `chatservice` should suffice.
Append `-addr :80` to specify a port:

```shell
./chatservice -addr :80
```
