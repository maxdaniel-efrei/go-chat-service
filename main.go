package main

import (
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

var addr = flag.String("addr", ":8080", "service address")

func main() {
	flag.Parse()
	r := mux.NewRouter()
	rooms := make(map[string]*ChatRoom)

	r.HandleFunc("/rooms", func(w http.ResponseWriter, r *http.Request) {
		//_, err := fmt.Fprintf(w, "Hello! Request: %s\n", r.URL.Path)

		roomDtos := make([]*ChatRoomDto, 0)

		for k, v := range rooms {
			roomDtos = append(roomDtos, v.dto(k))
		}

		w.Header().Add("Content-Type", "application/json")
		_, err := fmt.Fprintln(w, roomDtos)
		if err != nil {
			return
		}
	})

	r.HandleFunc("/rooms/{id}/messages", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		rooms[vars["id"]].serveWs(w, r)
	}).Methods(http.MethodGet).Queries("user")

	server := http.Server{
		Addr:              *addr,
		Handler:           r,
		ReadHeaderTimeout: 3 * time.Second,
	}
	err := server.ListenAndServe()
	log.Println("Listening on ", addr)
	if err != nil {
		log.Fatal("ListenAndServeError: ", err)
	}
}
