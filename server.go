package main

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

type ChatRoom struct {
	// registered clients
	clients map[*ChatClient]bool

	// channel of messages from clients
	broadcast chan []byte

	// channel to register clients
	register chan *ChatClient

	// channel to unregister clients
	unregister chan *ChatClient

	upgrader websocket.Upgrader
}

type ChatRoomDto struct {
	id    string
	users []string
}

func (room *ChatRoom) dto(id string) *ChatRoomDto {
	users := make([]string, 0)

	for k, _ := range room.clients {
		users = append(users, k.username)
	}

	return &ChatRoomDto{
		id:    id,
		users: users,
	}
}

func (room *ChatRoom) serveWs(w http.ResponseWriter, r *http.Request) {
	conn, err := room.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	username := r.URL.Query().Get("user")

	// add new client to room
	client := NewClient(room, conn, username)
	client.room.register <- client

	go client.read()
	go client.write()
}

func (room *ChatRoom) handleChannels() {
	for {
		select {
		// handle register channel
		case client := <-room.register:
			room.clients[client] = true
		// handle unregister channel
		case client := <-room.unregister:
			if _, ok := room.clients[client]; ok {
				// delete client
				delete(room.clients, client)
				close(client.send)
			}
		// handle broadcast channel
		case message := <-room.broadcast:
			for client := range room.clients {
				select {
				case client.send <- message:
				default:
					delete(room.clients, client)
					close(client.send)
				}
			}
		}
	}
}

func NewRoom() *ChatRoom {
	return &ChatRoom{
		broadcast:  make(chan []byte),
		register:   make(chan *ChatClient),
		unregister: make(chan *ChatClient),
		clients:    make(map[*ChatClient]bool),
		upgrader: websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		},
	}
}
